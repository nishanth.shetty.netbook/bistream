package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"time"

	pb "github.com/NishanthSpShettyShetty/bistream/proto"
	"google.golang.org/grpc"
)

type Job struct {
	name   string
	client string
}

//Clients hold the metadata of clients for
type Clients struct {
	m map[string]struct{}
}

func (c *Clients) register(id string) {
	c.m[id] = struct{}{}
}

func (c *Clients) registered(id string) bool {
	if _, ok := c.m[id]; ok {
		return ok
	}
	return false
}

// Server setup a server
type Server struct {
	client      *Clients
	subscribers map[string]pb.Streamer_ChatterServer
	jobQueue    chan Job
	// manage the job creator routine
	gotClient chan struct{}
	hasClient bool
	pb.UnimplementedStreamerServer
}

func New() pb.StreamerServer {
	c := Clients{
		m: make(map[string]struct{}),
	}
	server := &Server{
		client:      &c,
		subscribers: map[string]pb.Streamer_ChatterServer{},
		jobQueue:    make(chan Job),
		gotClient:   make(chan struct{}),
		hasClient:   false,
	}
	server.startWorker()
	server.startJobCreator()
	return server
}

// dummy random job creator
func (s *Server) startJobCreator() {
	go func() {
		//block until we have any client
		for {
			<-s.gotClient
			log.Println("randomly creating new jobs")
			for i := 1; i < 100000; i++ {
				time.Sleep(time.Second)
				all_clients := len(s.subscribers)
				if all_clients == 0 {
					log.Println("no agents to run job")
					s.hasClient = false
					break
				}

				id := rand.Intn(all_clients) + 1

				log.Println("sending jobs to client ", id)
				s.jobQueue <- Job{
					name:   fmt.Sprintf("job-%d", i),
					client: fmt.Sprintf("client_%d", id),
				}
			}
		}

	}()

}

func (s *Server) startWorker() {
	log.Println("starting async worker routine")
	go func() {

		//iterate over some queue which gets the random jobs
		for job := range s.jobQueue {

			for id, stream := range s.subscribers {
				if id == job.client {
					stream.Send(&pb.Chat{
						ClientID:  id,
						RequestId: "new-job",
						Kind:      pb.Kind_REQUEST,
						KindMeta: &pb.Chat_Req{
							Req: pb.RequestKind_JOB,
						},
						Payload: job.name,
					})
				}
			}
		}
	}()

}

func (s *Server) unsubscribe(id string) error {
	delete(s.subscribers, id)
	return nil
}

func (s *Server) subscribe(id string, streamer pb.Streamer_ChatterServer) error {

	if _, ok := s.subscribers[id]; ok {
		return errors.New("already subscribed")
	}
	s.subscribers[id] = streamer
	return nil
}

//RegisterClient why tho ?
func (s *Server) RegisterClient(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	log.Println("registering client")
	if s.client.registered(req.ID) {
		return nil, errors.New("client already registered")
	}
	s.client.register(req.ID)
	return &pb.RegisterResponse{}, nil
}

func (s *Server) GetClients(context.Context, *pb.GetClientRequest) (*pb.GetClientResponse, error) {
	ids := []string{}
	for k := range s.client.m {
		ids = append(ids, k)
	}
	return &pb.GetClientResponse{
		ClientID: ids,
	}, nil
}

func (s *Server) Chatter(streamer pb.Streamer_ChatterServer) error {

	chat, err := streamer.Recv()
	if err == io.EOF {
		log.Println("got EOF from client")
	}

	if err != nil {
		log.Println(" ===got ERROR ", err, " ignored")
	}
	log.Println("got chat ", chat)
	client := chat.ClientID
	s.subscribe(client, streamer)
	if !s.hasClient {
		s.hasClient = true
		s.gotClient <- struct{}{}
	}

	// go on chatting
	for {
		chat, err := streamer.Recv()
		if err == io.EOF {
			log.Println("got EOF from client")
			break
		}

		if streamer.Context().Err() != nil {
			break
		}
		if err != nil {
			log.Println(" ===got ERROR ", err, " ignored")
		}
		log.Println("> ", chat)

		if chat.Kind == pb.Kind_RESPONSE {
			// do nothing
		} else {

			streamer.Send(&pb.Chat{
				ClientID:  chat.ClientID,
				RequestId: chat.RequestId,
				Payload:   "",
				Kind:      pb.Kind_RESPONSE,
			})
		}
	}
	s.unsubscribe(client)

	return nil
}

func main() {
	log.Println("running server ...")
	server := New()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", 8080))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterStreamerServer(grpcServer, server)
	log.Println("listening on port 8080")
	grpcServer.Serve(lis)

}
