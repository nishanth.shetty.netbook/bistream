.PHONY: proto server agent
server:
	go run server/main.go 


agent:
	go run agent/main.go ${ARGS}

proto:
	@echo "generating proto code"
	zsh ./compile.sh


fmt-proto:
	clang-format --style=Chromium -i ./proto/*.proto
