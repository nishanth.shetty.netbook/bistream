package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	pb "github.com/NishanthSpShettyShetty/bistream/proto"
	"google.golang.org/grpc"
)

func main() {

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	n := 1
	if len(os.Args) == 2 {
		i := os.Args[1]
		num, err := strconv.Atoi(i)
		if err != nil {
			log.Panicln("invalid agent id")
		}
		n = num
	}

	id := fmt.Sprintf("client_%d", n)
	log.Println("running agent : ", id)
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Panicln("failed to connect to server ", err)
		return
	}

	streamer := pb.NewStreamerClient(conn)
	_, err = streamer.RegisterClient(context.Background(), &pb.RegisterRequest{

		ID: id,
	})
	if err != nil {
		log.Println("failed to register client", err)
	}

	chatter, err := streamer.Chatter(context.Background())

	if err != nil {
		log.Panicln("failed to start chatter")
	}

	go func() {
		s := <-sigc
		log.Println("got signal ", s)
		chatter.CloseSend()
	}()

	err = chatter.Send(&pb.Chat{
		ClientID:  id,
		RequestId: id,
		Kind:      pb.Kind_REQUEST,
		KindMeta:  &pb.Chat_Req{},
		Payload:   "hello",
	})
	if err != nil {
		log.Panicln("failed to send chat")
	}

	for {
		got, err := chatter.Recv()

		if err != nil {
			log.Panicln("failed to recv chat")
		}
		log.Println("R > ", got)
		if got.Kind == pb.Kind_RESPONSE {
			continue
		}
		got.Kind = pb.Kind_RESPONSE
		got.Payload = fmt.Sprintf("%s finished", got.Payload)
		log.Println("S < ", got)
		chatter.Send(got)
	}

}
